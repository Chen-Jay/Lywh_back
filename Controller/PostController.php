<?php

class PostController extends Controller
{
    public function handle()
    {
        global $json;

        switch($json['object'])
        {
            case 'activity':
                $model=new ActivityModel();
                /**
                 * 将具体数据从json里面解析出来
                 */
                $activityName=$json['activityName'];
                $userId=$json['userId'];
                $mType=$json['mType'];
                $time=$json['time'];
                $type=$json['type'];
                $budget=$json['budget'];
                $lamp=$json['lamp'];
                $description=$json['description'];

                $model->postActivity($activityName,$userId,$mType,$time,$type,$budget,$lamp,$description);
                break;
            case 'user':
                $model=new UserModel();
                $model->postUser();
                break;
            case 'sponsor':
                $model=new SponsorModel();
                $model->postSponsor();
                break;
            case 'university':
                $model=new UniversityModel();
                $model->postUniversity();
                break;
        }
    }
}
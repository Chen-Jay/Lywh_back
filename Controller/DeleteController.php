<?php

class DeleteController extends Controller
{
    public function handle()
    {
        global $json;

        switch($json['object'])
        {
            case 'activity':
                $model=new ActivityModel();
                /**
                 * 将具体数据从json里面解析出来
                 */
                $id=$json['activityId'];
                $model->deleteActivity($id);
                break;
            case 'user':
                $model=new UserModel();
                $model->deleteUser();
                break;
            case 'sponsor':
                $model=new SponsorModel();
                $model->deleteSponsor();
                break;
            case 'university':
                $model=new UniversityModel();
                $model->deleteUniversity();
                break;
        }
    }
}
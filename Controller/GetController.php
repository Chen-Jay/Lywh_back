<?php

class GetController extends Controller
{
    public function handle()
    {
        global $json;
        switch($json['object'])
        {
            case 'activity':
                $model=new ActivityModel();
                $model->getActivity();
                break;
            case 'user':
                $model=new UserModel();
                $model->getUser();
                break;
            case 'sponsor':
                $model=new SponsorModel();
                $model->getSponsor();
                break;
            case 'university':
                $model=new UniversityModel();
                $model->getUniversity();
                break;
        }
    }
}
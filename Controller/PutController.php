<?php

class PutController extends Controller
{
    public function handle()
    {
        global $json;

        switch($json['object'])
        {
            case 'activity':
                $model=new ActivityModel();
                $model->putActivity();
                break;
            case 'user':
                $model=new UserModel();
                $model->putUser();
                break;
            case 'sponsor':
                $model=new SponsorModel();
                $model->putSponsor();
                break;
        }
    }
}
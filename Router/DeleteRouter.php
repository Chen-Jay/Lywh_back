<?php

class DeleteRouter extends Router
{
    public function route()
    {
        global $json;

        $controller;
        /**
         * 根据不同的工作进行分配
         */
        $controller= new DeleteController();
        $controller->handle();
    }
}
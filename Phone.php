<?php

require_once 'InitialConfiguration.php';

Class Phone extends Root
{
    public function start()
    {
        /**
         * 使用自动加载器，自动将需要的php文件require进来
         */
        spl_autoload_register('autoloader'); //注册autoloader
        $ROOTPATH=$_SERVER['DOCUMENT_ROOT'].'/';  //将当前的初始路径储存为$WkPath，用于寻找其他php文件

        /**
         * 获取请求的URL并且进行拆解
         */
        $url=$_SERVER['REQUEST_URI'];
        $URL_Group=explode('/',$url);

        /**
         * 获取消息的主体部分,传递给全局的raw变量
         */
        global $raw;
        $raw = file_get_contents('php://input');

        /**
         * 将主体部分解析成JSON,传递给全局变量json
         */
        global $json;
        $json=json_decode($raw,true);

        /**
         * 验证第一个URL为对应的目录,并且创建对应的Router进行路由
         */
        if($URL_Group[1]=='lywh')
        {
            $router;
            switch($_SERVER['REQUEST_METHOD'])
            {
                // case 'GET':
                //     $router=new GetRouter();
                //     break;
                case 'POST':
                    if($json['method']=='get')
                    {
                        $router=new GetRouter();
                    }
                    else{
                        $router=new PostRouter();
                    }
                    break;
                case 'PUT':
                    if($json['method']=='get')
                    {
                        $router=new GetRouter();
                    }
                    else{
                        $router=new PutRouter();
                    }
                    break;
                case 'DELETE':
                    if($json['method']=='get')
                    {
                        $router=new GetRouter();
                    }
                    else
                    {
                        $router=new DeleteRouter();
                    }
                    break;
            }
            $router->route();
            
        }
    }

}

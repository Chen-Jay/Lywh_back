<?php

class SponsorModel extends Model
{
    public function postSponsor(){
        global $db;
        global $json;

        $data=$json['data'];
        $sql="INSERT INTO lywh.lywh_sponsor(lywh_sponsorPhoneNumber,lywh_budget,lywh_type,lywh_dateStart,lywh_dateEnd,lywh_description) VALUES (:phoneNumber,:budget,:type,:dateStart,:dateEnd,:description)";
        $array=array();
        $array['phoneNumber']=$data['sponsorPhoneNumber'];
        $array['budget']=$data['budget'];
        $array['type']=$data['type'];
        $array['dateStart']=$data['dateStart'];
        $array['dateEnd']=$data['dateEnd'];
        $array['description']=$data['description'];
        $stmt=$db->prepare($sql);
        $if_success=$stmt->execute($array);
        if($if_success==true)
        {
            $result=array();
            $result['result']='1';
            $js=json_encode($result);
            echo $js;
        }
        else{
            $result=array();
            $result['result']='0';
            $js=json_encode($result);
            echo $js;
        }
    }

    public function deleteSponsor(){

        global $db;
        global $json;
        $sql="DELETE FROM lywh.lywh_sponsor WHERE lywh_sponsorId=:sponsorId";
        $stmt=$db->prepare($sql);
        //echo $json['condition']['sponsorId'];
        $if_success=$stmt->execute(array('sponsorId'=>$json['condition']['sponsorId']));
        if($if_success==true)
        {
            $result=array();
            $result['result']='1';
            $js=json_encode($result);
            echo $js;
        }
        else{
            $result=array();
            $result['result']='0';
            $js=json_encode($result);
            echo $js;
        }
    }
    
    public function getSponsor(){
                /**
                 * 将具体数据从json里面解析出来
                 */
                global $json;
                global $db;
                $sponsorId=$json['condition']['sponsorId'];
                $phoneNumber=$json['condition']['account'];
                $dateStart=$json['condition']['dateStart'];
                $dateEnd=$json['condition']['dataEnd'];
                $type=$json['condition']['type'];
                $budget=$json['condition']['budget'];
                $lamp=$json['condition']['lamp'];
                $key=$json['order'];
                $number=$json['number'];
                $array=array();
                

                $sql="SELECT * FROM lywh.lywh_sponsor WHERE ";
                if($sponsorId !='')
                {
                    $sql=$sql.'lywh_sponsorId='.':sponsorId'.' and ';
                    $array['sponsorId']=$sponsorId;
                }
                if($phoneNumber !='')
                {
                    $sql=$sql.'lywh_sponsorPhoneNumber='.':phoneNumber'.' and ';
                    $array['phoneNumber']=$phoneNumber;
                }
                if($dateStart !='')
                {
                    $sql=$sql.'lywh_dateStart='.':dateStart'.' and ';
                    $array['dateStart']=$dateStart;
                }
                if($dateEnd !='')
                {
                    $sql=$sql.'lywh_dateEnd='.':dateEnd'.' and ';
                    $array['dateEnd']=$dateEnd;
                }
                if($type !='')
                {
                    $sql=$sql.'lywh_type='.':type'.' and ';
                    $array['type']=$type;
                }
                if($budget !='')
                {
                    $sql=$sql.'lywh_budget='.':mType'.' and ';
                    $array['budget']=$budget;
                }
                $sql=$sql.'1';
                if($key!='')
                {
                    $sql=$sql.' ORDER BY lywh_'.$key.' DESC';
                }
                $sql=$sql.' LIMIT '.$number;
                $stmt=$db->prepare($sql);
                $if_success=$stmt->execute($array);
               

                $result=$stmt->fetchAll(PDO::FETCH_ASSOC);
                echo(json_encode($result));
                
    }

    public function putSponsor(){
        ;
    }
}
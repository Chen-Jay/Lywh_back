<?php

class UserModel extends Model
{
    public function postUser()
    {
        
        global $db;
        global $json;
        $sql='INSERT INTO lywh.lywh_user(lywh_phoneNumber,lywh_nickName) VALUES (:phoneNum,:nickName)';
        $data=$json['data'];
        $array=array();
        $array['phoneNum']=$data['account'];
        // $array['company']=$data['workplace'];
        // $array['age']=$data['age'];
        // $array['gender']=$data['gender'];
        $array['nickName']=$data['nickname'];
        // $array['headpic']=$data['headpic'];
        $stmt=$db->prepare($sql);
        $if_success=$stmt->execute($array);
        
        $sql='INSERT INTO lywh.lywh_userPass(lywh_phoneNumber,lywh_password) VALUES (:phoneNum,:password)';
        $array2=array();
        $array2['phoneNum']=$data['account'];
        $array2['password']=$data['password'];
        $stmt=$db->prepare($sql);
        $if_success=$stmt->execute($array2);
        if($if_success==true)
        {
            $result=array();
            $result['result']='1';
            $js=json_encode($result);
            echo $js;
        }
        else{
            $result=array();
            $result['result']='0';
            $js=json_encode($result);
            echo $js;
        }
    }
    public function getUser(){
        global $json;
        global $db;

        $id=$json['condition']['account'];
        $sql="SELECT * FROM lywh.lywh_user WHERE 'id'=:id";
        $stmt=$db->prepare($sql);
        $if_success=$stmt->execute(array('id'=>$id));
        if($if_success==true)
        {
            $result=array();
            $result['result']='1';
            $js=json_encode($result);
            echo $js;
        }
        else{
            $result=array();
            $result['result']='0';
            $js=json_encode($result);
            echo $js;
        }
    }

    public function putUser(){
        global $json;
        global $db;
        $condition=$json['condition'];
        $data=$json['data'];
        if($data!='')
        {
            /**
             * 修改用户信息
             */
            $sql="UPDATE lywh.lywh_user SET lywh_nickName=:nickName,lywh_age=:age,lywh_sex=:gender,lywh_company=:company,lywh_headpic=:headpic WHERE lywh_phoneNumber=:phoneNumber";
            $array=array();
            $array['phoneNumber']=$condition['account'];
            $array['nickName']=$data['nickname'];
            $array['age']=$data['age'];
            $array['gender']=$data['gender'];
            $array['company']=$data['workplace'];
            $array['headpic']=$data['headpic'];
            $stmt=$db->prepare($sql);
            $if_success=$stmt->execute($array);
            
            /**
             * 修改用户密码
             */
            $sql='UPDATE lywh.lywh_userPass SET lywh_password=:password WHERE lywh_phoneNumber=:phoneNumber';
            $array2=array();
            $array2['phoneNumber']=$condition['account'];
            $array2['password']=$data['password'];
            $stmt=$db->prepare($sql);
            $if_success=$stmt->execute($array2);

            /**
             * 返回处理结果
             */
            if($if_success==false)
            {
                $result=array();
                $result['result']='0';
                $js=json_encode($result);
                echo $js;
            }
            else{
                $result=array();
                $result['result']='1';
                $js=json_encode($result);
                echo $js;
            }
        }
        else
        {
            
            /**
             * 进行登录验证
             */
            $sql="SELECT * FROM lywh.lywh_userPass WHERE lywh_password=:password and lywh_phoneNumber=:phoneNumber";
            $array2=array();
            $array2['phoneNumber']=$condition['account'];
            $array2['password']=$condition['password'];
            $stmt=$db->prepare($sql);
            $if_success=$stmt->execute($array2);
            $result_s=$stmt->fetchAll(PDO::FETCH_ASSOC);
            if(count($result_s) !=0)
            {
                $result=array();
                $result['result']='1';
                $js=json_encode($result);
                echo $js;
            }
            else
            {
                $result=array();
                $result['result']='0';
                $js=json_encode($result);
                echo $js;
            }
        }
    }
}
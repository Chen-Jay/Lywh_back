<?php

class ActivityModel extends Model
{


    public function postActivity($activityName,$userId,$mType,$time,$type,$budget,$lamp,$description){
        global $db;
        $sql='INSERT INTO lywh.lywh_activity(lywh_activityName,lywh_userId,lywh_mType,lywh_time,lywh_type,lywh_budget,lywh_lamp,lywh_description) VALUES(:lywh_activityName,:lywh_userId,:lywh_mType,:lywh_time,:lywh_type,:lywh_budget,:lywh_lamp,:lywh_description)';
        $stmt=$db->prepare($sql);
        $if_success=$stmt->execute(array('lywh_activityName'=>$activityName,'lywh_userId'=>$userId,'lywh_time'=>$time,'lywh_type'=>$type,'lywh_budget'=>$budget,'lywh_lamp'=>$lamp,'lywh_description'=>$description));
        if($if_success==true)
        {
            $result=array();
            $result['result']='1';
            $js=json_encode($result);
            echo $js;
        }
        else{
            $result=array();
            $result['result']='0';
            $js=json_encode($result);
            echo $js;
        }
    }

    public function deleteActivity($activityId){
        global $db;
        $sql="DELETE FROM lywh.lywh_activity WHERE 'lywh_activityId'=:activityId";
        $stmt=$db->prepare($sql);
        $if_success=$stmt->execute(array('activityId'=>$activityId));
        if($if_success==true)
        {
            $result=array();
            $result['result']='1';
            $js=json_encode($result);
            echo $js;
        }
        else{
            $result=array();
            $result['result']='0';
            $js=json_encode($result);
            echo $js;
        }
    }

    public function getActivity(){
                /**
                 * 将具体数据从json里面解析出来
                 */
                global $json;
                global $db;
                $activityId=$json['condition']['activityId'];
                $activityName=$json['condition']['activityName'];
                $mType=$json['condition']['mType'];
                $time=$json['condition']['time'];
                $type=$json['condition']['type'];
                $budget=$json['condition']['budget'];
                $lamp=$json['condition']['lamp'];
                $key=$json['order'];
                $number=$json['number'];
                $array=array();
                

                $sql="SELECT * FROM lywh.lywh_activity WHERE ";
                if($activityId !='')
                {
                    $sql=$sql.'lywh_activityId='.':activityId'.' and ';
                    $array['activityId']=$activityId;
                }
                if($activityName !='')
                {
                    $sql=$sql.'lywh_activityName='.':activityName'.' and ';
                    $array['activityName']=$activityName;
                }
                if($mType !='')
                {
                    $sql=$sql.'lywh_mType='.':mType'.' and ';
                    $array['mType']=$mType;
                }
                if($time !='')
                {
                    $sql=$sql.'lywh_time='.':time'.' and ';
                    $array['time']=$time;
                }if($type !='')
                {
                    $sql=$sql.'lywh_type='.':type'.' and ';
                    $array['type']=$type;
                }
                if($budget !='')
                {
                    $sql=$sql.'lywh_budget='.':mType'.' and ';
                    $array['budget']=$budget;
                }
                $sql=$sql.'1';
                if($key!='')
                {
                    $sql=$sql.' ORDER BY lywh_'.$key.' DESC';
                }
                $sql=$sql.' LIMIT '.$number;
                $stmt=$db->prepare($sql);
                $if_success=$stmt->execute($array);
               

                $result=$stmt->fetchAll(PDO::FETCH_ASSOC);
                echo(json_encode($result));
                


    }

    public function putActivity(){
        echo "去你妈的,没有这个功能.";
    }
}